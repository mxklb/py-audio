FROM mxklb/py-docker:2.0.0 AS builder

RUN apt-get update && apt-get install -y git build-essential liblo-dev libasound2-dev

# Due to portaudio error:
# https://stackoverflow.com/questions/59006083/how-to-install-portaudio-on-pi-properly/60824906#60824906
RUN git clone https://github.com/gglockner/portaudio -b alsapatch \
    && cd portaudio \
    && ./configure \
    && make \
    && make install \
    && ldconfig 

# Install build dependencies
RUN  pip3 install --upgrade pip && pip3 install numpy==2.2.* Cython==3.0.*

# Building wheels from source ..
RUN git clone https://git.aubio.org/aubio/aubio.git --recurse-submodules -b 0.4.6 && \
    cd aubio && python setup.py bdist_wheel && cp -a dist/. /tmp/wheels/

RUN git clone https://github.com/CPJKU/madmom.git --recurse-submodules && \
    cd madmom && git reset --hard 27f032e8947204902c675e5e341a3faf5dc86dae && \
    python setup.py bdist_wheel && cp -a dist/. /tmp/wheels/

RUN git clone https://github.com/CristiFati/pyaudio.git --recurse-submodules -b v0.2.13 && \
    cd pyaudio && python setup.py bdist_wheel && cp -a dist/. /tmp/wheels/

RUN git clone https://github.com/gesellkammer/pyliblo3.git --recurse-submodules -b v0.16.3 && \
    cd pyliblo3 && python setup.py bdist_wheel && cp -a dist/. /tmp/wheels/

RUN git clone https://github.com/patrickkidd/pyrtmidi.git --recurse-submodules -b v2.5.0 && \
    cd pyrtmidi && python setup.py bdist_wheel && cp -a dist/. /tmp/wheels/

FROM mxklb/py-docker:2.0.0

RUN apt-get update && apt-get install --no-install-recommends -y liblo-dev libasound2 libportaudio2

COPY --from=builder /tmp/wheels /tmp/wheels
RUN pip3 install /tmp/wheels/*.whl && rm -rf /tmp/wheels

COPY requirements.txt ./
RUN pip3 install -r requirements.txt && rm requirements.txt