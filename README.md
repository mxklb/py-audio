# py-audio

Simple Dockerfile to build and deploy python audio apps inside containers.

For installation details refer to the `Dockerfile`.
