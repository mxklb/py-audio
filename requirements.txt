matplotlib==3.*
cffi==1.*
numpy_ringbuffer==0.2.*
tabulate==0.9.*
mido==1.*
joblib==1.*